<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Registration</title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7, maximum-scale=3.0, minimum-scale=0.7">
    <link type="image/x-icon" rel="=shortcut icon" href="${pageContext.request.contextPath}/favicon/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/login&registration.css"/>
</head>
<body>
<c:choose>
    <c:when test="${requestScope.attribute == 'alreadyExists'}">
        <div class="error">
            <div class="registrationExistsError">
                User already exists
            </div>
        </div>
    </c:when>
    <c:when test="${requestScope.attribute == 'failPassword'}">
        <div class="error">
            <div class="registrationPasswordError">Your password must have a length of 6 to 52 characters
            </div>
        </div>
    </c:when>
    <c:when test="${requestScope.attribute == 'failLogin'}">
        <div class="error">
            <div class="registrationLoginError">
                Your login must have a length of 6 to 52 characters
            </div>
        </div>
    </c:when>
    <c:when test="${requestScope.attribute == 'failLastName'}">
        <div class="error">
            <div class="registrationNameError">
                Your lastname must contain the first capital letter, not contain any characters other than letters and
                have
                a length of 3 to 52 characters
            </div>
        </div>
    </c:when>
    <c:when test="${requestScope.attribute == 'failFirstName'}">
        <div class="error">
            <div class="registrationNameError">
                Your firstname must contain the first capital letter, not contain any characters other than letters and
                have
                a length of 3 to 52 characters
            </div>
        </div>
    </c:when>
</c:choose>

<div class="registration">
    <h3>Registration</h3>
    <form name="registrationForm" method="POST" action="${pageContext.request.contextPath}/reg">
        <div>
            <label for="firstname">First Name</label>
            <input id="firstname" type="text" name="firstName" value="" placeholder="type your first name"/>
        </div>
        <br>
        <div class="field4">
            <label for="lastname"> Last Name </label>
            <input id="lastname" type="text" name="lastName" value="" placeholder="type your last name"/>
        </div>
        <br>
        <div class="field2">
            <label for="login"> Login </label>
            <input id="login" type="text" name="login" value="" placeholder="type your login"/>
        </div>
        <br>
        <div class="field3">
            <label for="password"> Password </label>
            <input id="password" type="password" name="password" value="" placeholder="type your password"/>
        </div>
        <br>
        <div>
            <label for="country"> Country </label>
            <select id="country" name="country">
                <option value="=1">Belarus</option>
                <option value="=2">Russia</option>
                <option value="=3">Ukraine</option>
                <option value="=4">Kazakhstan</option>
            </select>
        </div>
        <br>
        <div>
            <label> Gender: </label>
            <label>
                <input type="radio" name="gender" value="Male" checked/>
            </label> Male
            <label>
                <input type="radio" name="gender" value="Female"/>
            </label> Female
        </div>
        <br>
        <div>
            <input class="button" type="submit" value="Sign up"/>
        </div>
    </form>


    <h4>Have an account?</h4>
    <p><a class="nonDecoration" href="${pageContext.request.contextPath}/jsp/login.jsp">Log in</a></p>
</div>
</body>
</html>
