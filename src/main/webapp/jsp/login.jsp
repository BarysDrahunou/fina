<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Log-in</title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=3.0, minimum-scale=0.8">
    <link type="image/x-icon" rel="=shortcut icon" href="${pageContext.request.contextPath}/favicon/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/login&registration.css"/>
</head>
<body>
<c:if test="${requestScope.isUserExists ==false}">
    <div class="error">
        <div class="loginError">
            Incorrect login or password
        </div>
    </div>
</c:if>

<div class="login">
    <h3>Login</h3>
    <form name="loginForm" method="POST" action="${pageContext.request.contextPath}/log" autocomplete="off">
        <div class="field1">
            <label for="login"> Login </label>
            <input id="login" type="text" name="login" value="" placeholder="login" required/>
        </div>
        <br>
        <div>
            <label for="password"> Password </label>
            <input id="password" type="password" name="password" value="" placeholder="password" required/>
        </div>
        <br><br>
        <input class="button" type="submit" value="Log in"/>
    </form>
    <h4>Don't have an account?</h4>
    <p><a class="nonDecoration" href="${pageContext.request.contextPath}/jsp/registration.jsp">Registration</a></p>
</div>
</body>
</html>