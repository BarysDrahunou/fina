<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>My TODO list</title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=3.0, minimum-scale=0.8">
    <link type="image/x-icon" rel="=shortcut icon" href="${pageContext.request.contextPath}/favicon/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/toDosmth.css"/>
</head>
<body>


<div class="hello">
    <c:if test="${sessionScope.containsKey('user')}">
        <c:out value="${'Hello, '+=sessionScope.get('user')+='!'}"/>
    </c:if>
</div>

<div>
    <form name="Log out" method="POST" action="${pageContext.request.contextPath}/quit">
        <input class="logout" type="submit" value="Log out"/>
    </form>
</div>

<a class="days" id="today" href="${pageContext.request.contextPath}/today">Today</a>
<a class="days" id="tomorrow" href="${pageContext.request.contextPath}/tomorrow">Tomorrow</a>
<a class="days" id="someday" href="${pageContext.request.contextPath}/someDays">SomeDays</a>
<a class="days" id="recycler" href="${pageContext.request.contextPath}/recycler">Recycler</a>

<div id="tasks">
    <c:if test="${sessionScope.containsKey('empty')}">
        <h3 class="empty">${sessionScope.get('empty')}</h3>
    </c:if>
    <c:if test="${sessionScope.containsKey('listOfTasks')}">
        <table>
            <tr>
                <th id="currentDate">Date</th>
                <th id="task">Task</th>
                <th colspan="2" id="files">File</th>
                <th id="complete">Complete task</th>
                <th id="remove">Remove task</th>
            </tr>
            <c:forEach items="#{sessionScope.listOfTasks}" var="a">
                <tr>
                    <td><c:out value="${a.getDate()}"/></td>
                    <td><c:out value="${a.getTask()}"/></td>

                    <c:if test="${a.getBlob()}">
                        <td>
                            <form method="POST"
                                  action="${pageContext.request.contextPath}/getFile">
                                <button class="button" type="submit" value="${a.getID()}"
                                        name="getFile" formtarget="_blank" title="${a.getNameOfFile()}">Get file
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="POST"
                                  action="${pageContext.request.contextPath}/restoreOrRemove">
                                <button class="button" type="submit" value="${a.getID()}" name="deleteFile">Delete
                                    file
                                </button>
                            </form>
                        </td>
                    </c:if>
                    <c:if test="${!a.getBlob()}">

                        <td colspan="2">
                            <form method="POST" enctype="multipart/form-data"
                                  action="${pageContext.request.contextPath}/addFile" autocomplete="off">
                                <div class="fileUpload">
                                    <label><input id="fileInput" type="file" name="${a.getID()}">Select file</label>
                                    <button class="button" type="submit">Add file</button>
                                </div>


                            </form>
                        </td>
                    </c:if>

                    <td>
                        <form method="POST"
                              action="${pageContext.request.contextPath}/restoreOrRemove">
                            <button class="button" type="submit" value="${a.getID()}" name="complete">Complete</button>
                        </form>
                    </td>
                    <td>
                        <form method="POST"
                              action="${pageContext.request.contextPath}/restoreOrRemove">
                            <button class="button" type="submit" value="${a.getID()}" name="remove">Remove</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${sessionScope.containsKey('listOfRemovedTasks')}">
        <table>
            <tr>
                <th>Task</th>
                <th class="restoreOrRemoveFromRecycler">Restore task</th>
                <th class="restoreOrRemoveFromRecycler">Remove task</th>
                <th class="restoreAll">
                    <form method="POST"
                          action="${pageContext.request.contextPath}/restoreAllTasksFromRecycler">
                        <button class="button" type="submit">Restore All</button>
                    </form>
                </th>
                <th class="restoreAll">
                    <form method="POST"
                          action="${pageContext.request.contextPath}/removeAllTasksFromRecycler">
                        <button class="button" type="submit">Remove All</button>
                    </form>
                </th>
            </tr>
            <c:forEach items="#{sessionScope.listOfRemovedTasks}" var="a">
                <tr>
                    <td class="removedTasks"><c:out value="${a.getTask()}"/></td>
                    <td>
                        <form method="POST"
                              action="${pageContext.request.contextPath}/restoreOrRemoveFromRecycler">
                            <button class="button" type="submit" value="${a.getID()}" name="restoreTask">Restore
                            </button>
                        </form>
                    </td>
                    <td>
                        <form method="POST"
                              action="${pageContext.request.contextPath}/restoreOrRemoveFromRecycler">
                            <button class="button" type="submit" value="${a.getID()}" name="removeTask">Remove</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>


<div class="fileUpload" id="add">
    <form method="POST" enctype="multipart/form-data" action="${pageContext.request.contextPath}/add"
          autocomplete="off">
        <label>Add a new task
            <input class="space" type="text" name="Task" placeholder="Task" required/>
        </label>
        <label>Date
            <input class="space" type="date" name="Date" required/>
        </label>
        <label>Attach your file
            <input id="addFile" class="space" type="file" name="File"/>
        </label>
        <button class="button" type="submit">Add task</button>
    </form>
</div>

<div id="date">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js">
    </script>
</div>
</body>
</html>
