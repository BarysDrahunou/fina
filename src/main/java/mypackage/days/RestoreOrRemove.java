package mypackage.days;

import mypackage.operationswithtasks.CurrentDayIsEmpty;
import mypackage.tasks.TaskDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;


public class RestoreOrRemove extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession httpSession = request.getSession();
        TaskDAO taskDAO = new TaskDAO();
        if (Objects.nonNull(request.getParameter("complete"))) {
            taskDAO.changeStatus(2, Integer.parseInt(request.getParameter("complete")), request,0);
        }
        if (Objects.nonNull(request.getParameter("remove"))) {
            taskDAO.changeStatus(1, Integer.parseInt(request.getParameter("remove")), request,0);
        }

        if (Objects.nonNull(request.getParameter("deleteFile"))) {
            taskDAO.updateFile(null, null, Integer.parseInt(request.getParameter("deleteFile")), request," IS NOT NULL");
        }
        new CurrentDayIsEmpty().checkIsEmpty(taskDAO, request, httpSession);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/toDosmth.jsp");
        requestDispatcher.forward(request, response);
    }
}
