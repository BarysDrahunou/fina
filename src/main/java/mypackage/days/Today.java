package mypackage.days;

import mypackage.operationswithtasks.GetAllCurrentTasks;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Today extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        new GetAllCurrentTasks().getAllCurrentTasks(request, response, "today");
    }
}
