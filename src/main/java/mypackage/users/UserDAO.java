package mypackage.users;

import mypackage.connection.Connect;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class UserDAO implements UsersDAO {

    @Override
    public void add(User user, HttpServletRequest request) {
        String sql = "INSERT INTO USERS (FIRSTNAME, LASTNAME, LOGIN, PASSWORD, COUNTRY) VALUES(?, ?, ?, ?,?)";
        try {
            Connect connect = new Connect();
            Connection connection = connect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getCountry());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isUserExists(String login, String password, HttpServletRequest request) {
        return !Objects.isNull(getUser(login, password, request));
    }

    @Override
    public User getUser(String login, String password, HttpServletRequest request) {
        User user = new User();
        Connect connect = new Connect();
        String sql = "SELECT FIRSTNAME, LASTNAME, LOGIN, PASSWORD,COUNTRY FROM USERS WHERE LOGIN=? AND PASSWORD=?";
        try (Connection connection = connect.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setFirstName(resultSet.getString("FIRSTNAME"));
                user.setLastName(resultSet.getString("LASTNAME"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setCountry(resultSet.getString("COUNTRY"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}

