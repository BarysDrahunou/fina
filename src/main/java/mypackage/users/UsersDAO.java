package mypackage.users;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

public interface UsersDAO {
    void add(User user, HttpServletRequest request);

    boolean isUserExists(String login, String password, HttpServletRequest request) throws SQLException;

    User getUser(String login, String password, HttpServletRequest request) throws SQLException;
}
