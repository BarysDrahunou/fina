package mypackage.loginlogoutandregistration;

import mypackage.connection.Connect;
import mypackage.operationswithtasks.GetAllCurrentTasks;
import mypackage.tasks.TaskDAO;
import mypackage.users.UserDAO;
import mypackage.users.UsersDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        UsersDAO usersDAO = new UserDAO();
        RequestDispatcher requestDispatcher;
        try {
            if (usersDAO.isUserExists(login, password, request)) {
                synchronized (Thread.currentThread()) {
                    Connect connect = new Connect();
                    new TaskDAO().setMaxAllowedPacket(connect, 1073741824);
                    Connection connection = connect.getConnection();
                    HttpSession httpSession = request.getSession();
                    httpSession.setAttribute("connection", connection);
                    httpSession.setAttribute("user", usersDAO.getUser(login, password, request));
                    httpSession.setAttribute("today", "today");
                    new GetAllCurrentTasks().getAllCurrentTasks(request, response, (String) httpSession.getAttribute("today"));
                }
            } else {
                request.setAttribute("isUserExists", false);
                requestDispatcher = request.getRequestDispatcher("jsp/login.jsp");
                requestDispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

