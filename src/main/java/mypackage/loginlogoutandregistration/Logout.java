package mypackage.loginlogoutandregistration;

import mypackage.users.User;

import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class Logout extends HttpServlet {
    @SuppressWarnings("ResultOfMethodCallIgnored")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession httpSession = request.getSession();
        String path = String.format("../webapps%s/files/%s/", request.getContextPath(), ((User) httpSession.getAttribute("user")).getLogin());
        File[] files = new File(path).listFiles();
        if (Objects.nonNull(files)) {
            for (File myFile : files) {
                myFile.delete();
            }
        }
        File file = new File(path);
        file.delete();
        Connection connection = (Connection) httpSession.getAttribute("connection");
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        httpSession.invalidate();
        response.sendRedirect("jsp/login.jsp");
    }
}
