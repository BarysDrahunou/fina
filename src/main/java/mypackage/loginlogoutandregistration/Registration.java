package mypackage.loginlogoutandregistration;

import mypackage.operationswithtasks.CheckPossibilityOfRegistration;
import mypackage.users.User;
import mypackage.users.UserDAO;
import mypackage.users.UsersDAO;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Registration extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        RequestDispatcher requestDispatcher;
        CheckPossibilityOfRegistration checkPossibilityOfRegistration = new CheckPossibilityOfRegistration();
        if (checkPossibilityOfRegistration.isYourInformationCorrect(request)) {
            synchronized (Thread.currentThread()) {
                UsersDAO usersDAO = new UserDAO();
                User user = new User();
                user.setFirstName(checkPossibilityOfRegistration.getFirstName());
                user.setLastName(checkPossibilityOfRegistration.getLastName());
                user.setLogin(checkPossibilityOfRegistration.getLogin());
                user.setPassword(checkPossibilityOfRegistration.getPassword());
                user.setCountry(checkPossibilityOfRegistration.getCountry());
                usersDAO.add(user,request);
                requestDispatcher = request.getRequestDispatcher("jsp/login.jsp");
            }
        } else {
            requestDispatcher = request.getRequestDispatcher("jsp/registration.jsp");
        }
        requestDispatcher.forward(request, response);
    }
}
