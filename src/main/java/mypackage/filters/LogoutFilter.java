package mypackage.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

public class LogoutFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession httpSession = httpServletRequest.getSession();
        if ((!Objects.isNull(httpSession.getAttribute("user")))) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/toDosmth.jsp");
            requestDispatcher.forward(httpServletRequest, response);
        }
        filterChain.doFilter(request, response);
    }
}