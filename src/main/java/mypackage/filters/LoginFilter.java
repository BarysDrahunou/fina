package mypackage.filters;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession httpSession = httpServletRequest.getSession();
        String servletPath = httpServletRequest.getServletPath();
        Matcher matcher = Pattern.compile("html|css|jpg|woff|reg|log|ico").matcher(servletPath);
        if (!(matcher.find())) {
            if (Objects.isNull(httpSession.getAttribute("user"))) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/login.jsp");
                requestDispatcher.forward(request, response);
            }
        }
        filterChain.doFilter(request, response);
    }
}