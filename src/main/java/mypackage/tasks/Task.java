package mypackage.tasks;


import java.time.LocalDate;
import java.sql.Date;
import java.util.Objects;

public class Task {
    private int id;
    private Date date;
    private String task;
    private String nameOfFile;

    public Task(int id, Date date, String task,String nameOfFile) {
        this.id = id;
        this.date = date;
        this.task = task;
        this.nameOfFile = nameOfFile;
    }

    public Task(int id, String task) {
        this.id = id;
        this.task = task;
        date = Date.valueOf(LocalDate.now());
    }

    @SuppressWarnings("unused")
    public Date getDate() {
        if (LocalDate.now().compareTo(date.toLocalDate()) > 0) {
            date = Date.valueOf(LocalDate.now());
        }
        return date;
    }

    @SuppressWarnings("unused")
    public String getTask() {
        return task;
    }

    public int getID() {
        return id;
    }

    @SuppressWarnings("unused")
    public boolean getBlob() {
        return Objects.nonNull(nameOfFile);
    }

    @SuppressWarnings("unused")
    public String getNameOfFile() {
        return nameOfFile;
    }

}
