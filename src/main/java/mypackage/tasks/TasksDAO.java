package mypackage.tasks;

import mypackage.connection.Connect;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public interface TasksDAO {

    ArrayList<Task> getListOfTasks();

    void add(String arg, String nameOfFile, java.sql.Date date, byte[] arrayOfByte, HttpServletRequest request);

    List<Object> getFile(String sql, int id, HttpServletRequest request);

    void setMaxAllowedPacket(Connect connect, long sizeOfPacket);

    void getAllTasks(String sql, java.sql.Date date, int status, HttpServletRequest request);

    void getAllRemovedTasks(String sql, int status, HttpServletRequest request);

    void deleteTask(int id, HttpServletRequest request, int oldStatus);

    void changeStatus(int newStatus, int id, HttpServletRequest request, int oldStatus);

    void updateFile(byte[] bytes, String nameOfFile, int id, HttpServletRequest request, String status);

}
