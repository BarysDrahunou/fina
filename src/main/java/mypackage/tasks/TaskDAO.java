package mypackage.tasks;

import mypackage.connection.Connect;
import mypackage.users.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskDAO implements TasksDAO {


    private ArrayList<Task> listOfTasks = new ArrayList<>();

    @Override
    public ArrayList<Task> getListOfTasks() {
        return listOfTasks;
    }

    @Override
    public void setMaxAllowedPacket(Connect connect, long sizeOfPacket) {
        String sql = "SET GLOBAL MAX_ALLOWED_PACKET = ?";
        try (Connection connection = connect.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, sizeOfPacket);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(String arg, String nameOfFile, java.sql.Date date, byte[] arrayOfByte, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String sql = "INSERT INTO TODOLIST (DATETODO, NAMETODO, STATUSTODO, FNTODO,USERID,NAMEOFFILE) VALUES(?, ?, ?, ?, ?,?)";
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDate(1, date);
            preparedStatement.setString(2, arg);
            preparedStatement.setByte(3, (byte) 0);
            Blob blob = null;
            if (Objects.nonNull(arrayOfByte)) {
                blob = connection.createBlob();
                blob.setBytes(1, arrayOfByte);
            }
            preparedStatement.setBlob(4, blob);
            preparedStatement.setInt(5, ((User) httpSession.getAttribute("user")).getId(request));
            preparedStatement.setString(6, nameOfFile);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Object> getFile(String sql, int id, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        List<Object> list = new ArrayList<>();
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Blob blob = resultSet.getBlob("FNTODO");
                list.add(blob);
                list.add(resultSet.getString("NAMEOFFILE"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void getAllTasks(String sql, java.sql.Date date, int status, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDate(1, date);
            preparedStatement.setInt(2, ((User) httpSession.getAttribute("user")).getId(request));
            preparedStatement.setInt(3, status);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfTasks.add(new Task(resultSet.getInt("ID"), resultSet.getDate("DATETODO"), resultSet.getString("NAMETODO"), resultSet.getString("NAMEOFFILE")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAllRemovedTasks(String sql, int status, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String string = "UPDATE TODOLIST SET DATETODO = ? WHERE USERID =? and STATUSTODO=?";
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(string);
            PreparedStatement preparedStatement1 = connection.prepareStatement(sql);
            preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
            preparedStatement.setInt(2, ((User) httpSession.getAttribute("user")).getId(request));
            preparedStatement.setInt(3, status);
            preparedStatement.executeUpdate();
            preparedStatement1.setInt(1, ((User) httpSession.getAttribute("user")).getId(request));
            preparedStatement1.setInt(2, status);
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()) {
                listOfTasks.add(new Task(resultSet.getInt("ID"), resultSet.getString("NAMETODO")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeStatus(int newStatus, int id, HttpServletRequest request, int oldStatus) {
        HttpSession httpSession = request.getSession();
        String sql = "UPDATE TODOLIST SET STATUSTODO = ? WHERE USERID =? AND ID=? AND STATUSTODO=?";
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, newStatus);
            preparedStatement.setInt(2, ((User) httpSession.getAttribute("user")).getId(request));
            preparedStatement.setInt(3, id);
            preparedStatement.setInt(4, oldStatus);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTask(int id, HttpServletRequest request, int oldStatus) {

        HttpSession httpSession = request.getSession();
        String sql = "DELETE FROM TODOLIST WHERE ID=? AND STATUSTODO=?";
        try {
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, oldStatus);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void update(byte[] bytes, String nameOfFile, int id, String sql, HttpServletRequest request) {
        try {
            HttpSession httpSession = request.getSession();
            Connection connection = (Connection) httpSession.getAttribute("connection");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            Blob blob = null;
            if (Objects.nonNull(bytes)) {
                blob = connection.createBlob();
                blob.setBytes(1, bytes);
            }
            preparedStatement.setBlob(1, blob);
            preparedStatement.setString(2, nameOfFile);
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateFile(byte[] bytes, String nameOfFile, int id, HttpServletRequest request, String status) {
        String sql = String.format("UPDATE TODOLIST SET FNTODO = ?, NAMEOFFILE=? WHERE ID=? AND NAMEOFFILE%s", status);
        update(bytes, nameOfFile, id, sql, request);
    }

}

