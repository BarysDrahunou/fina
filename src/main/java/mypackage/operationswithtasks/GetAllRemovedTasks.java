package mypackage.operationswithtasks;

import mypackage.tasks.TaskDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class GetAllRemovedTasks {
    public void getAllRemovedTasks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TaskDAO taskDAO = new TaskDAO();
        String sqlQuery = "SELECT ID,DATETODO, NAMETODO,USERID FROM TODOLIST WHERE USERID=? AND STATUSTODO=?";
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("today", "recycler");
        httpSession.removeAttribute("listOfTasks");
        httpSession.removeAttribute("empty");
        httpSession.removeAttribute("listOfRemovedTasks");
        taskDAO.getAllRemovedTasks(sqlQuery, 1, request);
        if (taskDAO.getListOfTasks().size() != 0) {
            httpSession.setAttribute("listOfRemovedTasks", taskDAO.getListOfTasks());
        } else {
            httpSession.setAttribute("empty", "Your recycler is empty!");
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/toDosmth.jsp");
        requestDispatcher.forward(request, response);
    }
}
