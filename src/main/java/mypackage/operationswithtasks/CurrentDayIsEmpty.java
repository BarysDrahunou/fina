package mypackage.operationswithtasks;

import mypackage.tasks.TaskDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

public class CurrentDayIsEmpty {
    public void checkIsEmpty(TaskDAO taskDAO, HttpServletRequest request, HttpSession httpSession) {
        httpSession.removeAttribute("listOfTasks");
        httpSession.removeAttribute("empty");
        httpSession.removeAttribute("listOfRemovedTasks");
        String arg = httpSession.getAttribute("today").equals("today") ? "<" : httpSession.getAttribute("today").equals("tomorrow") ? "=" : httpSession.getAttribute("today").equals("someDays") ? ">" : null;
        if (Objects.nonNull(arg)) {
            String sqlQuery = String.format("SELECT ID,DATETODO,NAMETODO,STATUSTODO,USERID,NAMEOFFILE FROM TODOLIST WHERE DATETODO %s? AND USERID=? AND STATUSTODO=?", arg);
            taskDAO.getAllTasks(sqlQuery, Date.valueOf(LocalDate.now().plusDays(1)), 0, request);
            if (taskDAO.getListOfTasks().size() != 0) {
                httpSession.setAttribute("listOfTasks", taskDAO.getListOfTasks());
            } else {
                httpSession.setAttribute("empty", "You are lucky! You day is free!");
            }
        } else {
            String sqlQuery = "SELECT ID,DATETODO, NAMETODO,USERID FROM TODOLIST WHERE USERID=? AND STATUSTODO=?";
            taskDAO.getAllRemovedTasks(sqlQuery, 1, request);
            if (taskDAO.getListOfTasks().size() != 0) {
                httpSession.setAttribute("listOfRemovedTasks", taskDAO.getListOfTasks());
            } else {
                httpSession.setAttribute("empty", "Your recycler is empty!");
            }
        }
    }
}
