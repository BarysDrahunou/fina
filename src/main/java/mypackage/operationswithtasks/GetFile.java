package mypackage.operationswithtasks;

import mypackage.tasks.TaskDAO;
import mypackage.users.User;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;


public class GetFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession httpSession = request.getSession();
        String login = ((User) httpSession.getAttribute("user")).getLogin();
        int id = Integer.parseInt(request.getParameter("getFile"));
        String sqlQuery = "SELECT FNTODO, NAMEOFFILE  FROM TODOLIST WHERE ID=?";
        TaskDAO taskDAO = new TaskDAO();
        List<Object> list = taskDAO.getFile(sqlQuery, id, request);
        Blob blob = (Blob) list.get(0);
        String nameOfFile = (String) list.get(1);
        Path path = Files.createDirectories(Paths.get(String.format("../webapps%s/files/%s", request.getContextPath(), login)));
        File file = new File(String.format("%s/%s", path, nameOfFile));
        InputStream is;
        byte[] buffer;
        try (FileOutputStream fos = new FileOutputStream(file)) {
            if (blob.length() > 1000000) {
                buffer = new byte[1024];
            } else {
                buffer = new byte[1];
            }
            is = blob.getBinaryStream();
            while (is.read(buffer) > 0) {
                fos.write(buffer);
            }
            response.sendRedirect(String.format("files/%s/%s", login, nameOfFile));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            response.sendRedirect("jsp/toDosmth.jsp");
        }
    }
}
