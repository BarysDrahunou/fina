package mypackage.operationswithtasks;

import mypackage.tasks.TaskDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AddTask extends HttpServlet {
    @SuppressWarnings("ResultOfMethodCallIgnored")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String task;
        String date;
        String fileName;
        byte[] targetArray = null;
        try {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            task = items.get(0).getString();
            date = items.get(1).getString();
            fileName = FilenameUtils.getName(items.get(2).getName());
            if (Objects.nonNull(fileName)) {
                InputStream is = items.get(2).getInputStream();
                targetArray = new byte[is.available()];
                is.read(targetArray);
            }
        } catch (FileUploadException e) {
            throw new ServletException("Cannot parse multipart request.", e);
        }
        if (targetArray.length == 0) {
            targetArray = null;
            fileName = null;
        }
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date1 != null;
        java.sql.Date sqlDate = new java.sql.Date(date1.getTime());
        TaskDAO taskDAO = new TaskDAO();
        taskDAO.add(task, fileName, sqlDate, targetArray, request);
        new CurrentDayIsEmpty().checkIsEmpty(taskDAO, request, request.getSession());
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/toDosmth.jsp");
        requestDispatcher.forward(request, response);
    }
}
