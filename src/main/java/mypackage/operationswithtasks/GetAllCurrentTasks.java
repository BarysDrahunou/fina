package mypackage.operationswithtasks;

import mypackage.tasks.TaskDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class GetAllCurrentTasks {
    public void getAllCurrentTasks(HttpServletRequest request, HttpServletResponse response, String string) throws IOException {
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("today", string);
        new CurrentDayIsEmpty().checkIsEmpty(new TaskDAO(), request, httpSession);
        response.sendRedirect("jsp/toDosmth.jsp");
    }
}
