package mypackage.operationswithtasks;

import mypackage.connection.Connect;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckPossibilityOfRegistration {
    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private String country;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getCountry() {
        return country;
    }

    private void setErrorMessage(HttpServletRequest request, boolean isThisMistakePresents, String argument) {
        if (!isThisMistakePresents) {
            request.setAttribute("attribute", argument);
        }
    }

    public boolean isYourInformationCorrect(HttpServletRequest request) {
        firstName = request.getParameter("firstName");
        lastName = request.getParameter("lastName");
        login = request.getParameter("login");
        password = request.getParameter("password");
        country = request.getParameter("country");
        Pattern pattern = Pattern.compile("^[A-Z][a-z]{2,51}$");
        Matcher firstNameMatcher = pattern.matcher(firstName);
        Matcher lastNameMatcher = pattern.matcher(lastName);
        Pattern pattern1 = Pattern.compile("^.{6,52}$");
        Matcher loginMatcher = pattern1.matcher(login);
        Matcher passwordMatcher = pattern1.matcher(password);

        Connect connect = new Connect();
        String sql = "SELECT LOGIN FROM tododatabase.users WHERE LOGIN=? ";
        try (Connection connection = connect.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                request.setAttribute("attribute", "alreadyExists");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setErrorMessage(request, passwordMatcher.find(), "failPassword");
        setErrorMessage(request, loginMatcher.find(), "failLogin");
        setErrorMessage(request, lastNameMatcher.find(), "failLastName");
        setErrorMessage(request, firstNameMatcher.find(), "failFirstName");
        return Objects.isNull(request.getAttribute("attribute"));
    }
}
