package mypackage.operationswithtasks;


import mypackage.tasks.TaskDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AddFile extends HttpServlet {
    @SuppressWarnings("ResultOfMethodCallIgnored")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id;
        String fileName;
        byte[] targetArray;
        try {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            fileName = FilenameUtils.getName(items.get(0).getName());
            id = Integer.parseInt(items.get(0).getFieldName());
            InputStream is = items.get(0).getInputStream();
            targetArray = new byte[is.available()];
            is.read(targetArray);
        } catch (FileUploadException e) {
            throw new ServletException("Cannot parse multipart request.", e);
        }
        TaskDAO taskDAO = new TaskDAO();
        if (targetArray.length > 0) {
            taskDAO.updateFile(targetArray, fileName, id, request, " IS NULL");
        }
        new CurrentDayIsEmpty().checkIsEmpty(taskDAO, request, request.getSession());
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/toDosmth.jsp");
        requestDispatcher.forward(request, response);
    }
}
