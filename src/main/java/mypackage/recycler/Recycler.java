package mypackage.recycler;

import mypackage.operationswithtasks.GetAllRemovedTasks;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Recycler extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        new GetAllRemovedTasks().getAllRemovedTasks(request, response);
    }
}
