package mypackage.recycler;

import mypackage.operationswithtasks.GetAllRemovedTasks;
import mypackage.tasks.TaskDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;


public class RestoreOrRemoveFromRecycler extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        TaskDAO taskDAO = new TaskDAO();
        if (Objects.nonNull(request.getParameter("restoreTask"))) {
            taskDAO.changeStatus(0, Integer.parseInt(request.getParameter("restoreTask")), request,1);
        }

        if (Objects.nonNull(request.getParameter("removeTask"))) {
            taskDAO.deleteTask(Integer.parseInt(request.getParameter("removeTask")), request,1);
        }
        GetAllRemovedTasks getAllRemovedTasks = new GetAllRemovedTasks();
        getAllRemovedTasks.getAllRemovedTasks(request, response);
    }
}
