package mypackage.recycler;

import mypackage.tasks.Task;
import mypackage.tasks.TaskDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RestoreAllTasksFromRecycler extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession httpSession = request.getSession();
        TaskDAO taskDAO = new TaskDAO();
        String sqlQuery = "SELECT ID,NAMETODO,USERID  FROM TODOLIST WHERE USERID=? AND STATUSTODO=?";
        taskDAO.getAllRemovedTasks(sqlQuery, 1, request);
        for (Task task : taskDAO.getListOfTasks()) {
            taskDAO.changeStatus(0, task.getID(), request,1);
        }
        httpSession.removeAttribute("listOfRemovedTasks");
        httpSession.setAttribute("empty", "Your recycler is empty!");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/toDosmth.jsp");
        requestDispatcher.forward(request, response);
    }
}
